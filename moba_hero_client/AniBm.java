package ani;

import game.GameTool;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class AniBm {
	public int state;

	public int frame;// 当前步数
	public int maxframe;// 步数
	// ////////////////
	public int x;
	public int y;
	public int r;
	
	public String name;
	public ArrayList<Image> bmlist;
	
	public AniBm( )
	{
		this.frame=0;
		this.maxframe=10;
	}
	public AniBm(ArrayList<Image> bmlist)
	{
		this.frame=0;
		
		this.bmlist=bmlist;		
		
		this.maxframe= bmlist.size();
		
		state = AniBmData.READY;
		//System.out.println("ani bm: "+ this.bmlist.size());
	}
	
	public void setxy(int x,int y)
	{
		this.x=x;
		this.y=y; 
		//System.out.println("ani pos "+x+","+y);
		
	}
	public void setr(int r)
	{
		this.r=r;
		
	}
	public void draw()
	{
		GameTool.g.drawImage(bmlist.get(frame), x-r,y-r,null);
	}
	public void startplay()
	{
		frame=0;
		state = AniBmData.PLAY;
	}
	public void nextframe()
	{
		if(frame<maxframe-1)
			frame++;
		else
			state = AniBmData.STOP;
	}
}
